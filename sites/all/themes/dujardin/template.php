<?php

	/*
	 * Here we override the default HTML output of drupal.
	 * refer to http://drupal.org/node/550722
	 */

// Auto-rebuild the theme registry during theme development.
	if (theme_get_setting('clear_registry')) {
		// Rebuild .info data.
		system_rebuild_theme_data();
		// Rebuild theme registry.
		drupal_theme_rebuild();
	}

	/**
	 * Preprocesses the wrapping HTML.
	 *
	 * @param $vars
	 * @internal param array $variables Template variables.*   Template variables.
	 */
	function dujardin_preprocess_html(&$vars) {

		drupal_add_js('https://maps.googleapis.com/maps/api/js?v=3', [
			'type' => 'external',
			'scope' => 'header'
		]);

		drupal_add_css('https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css', [
			'type' => 'external'
		]);

		// Setup IE meta tag to force IE rendering mode
		$meta_ie_render_engine = array(
			'#type' => 'html_tag',
			'#tag' => 'meta',
			'#attributes' => array(
				'content' => 'IE=edge,chrome=1',
				'http-equiv' => 'X-UA-Compatible',
			)
		);
		//  Mobile viewport optimized: h5bp.com/viewport
		$meta_viewport = array(
			'#type' => 'html_tag',
			'#tag' => 'meta',
			'#attributes' => array(
				'content' => 'width=device-width',
				'name' => 'viewport',
			)
		);

		// Add header meta tag for IE to head
		drupal_add_html_head($meta_ie_render_engine, 'meta_ie_render_engine');
		drupal_add_html_head($meta_viewport, 'meta_viewport');
	}

	function dujardin_preprocess_page(&$vars, $hook) {
		if (isset($vars['node_title'])) {
			$vars['title'] = $vars['node_title'];
		}
		// Adding a class to #page in wireframe mode
		if (theme_get_setting('wireframe_mode')) {
			$vars['classes_array'][] = 'wireframe-mode';
		}
		// Adding classes wether #navigation is here or not
		if (!empty($vars['main_menu']) or !empty($vars['sub_menu'])) {
			$vars['classes_array'][] = 'with-navigation';
		}
		if (!empty($vars['secondary_menu'])) {
			$vars['classes_array'][] = 'with-subnav';
		}
		// Slide Show
		$slide_show = db_select('node', 'n')
			->fields('n', ['nid'])
			->condition('n.type', 'slideshow')
			->condition('n.status', 1)
			->execute();
		if($slide_show->rowCount()){
			$vars['slide_show'] = array_map(function($e){
				return node_view(node_load($e));
			}, $slide_show->fetchCol());
		}
	}

	function dujardin_preprocess_node(&$vars) {
		// Add a striping class.
		$vars['classes_array'][] = 'node-' . $vars['zebra'];
	}

	function dujardin_preprocess_block(&$vars, $hook) {
		// Add a striping class.
		$vars['classes_array'][] = 'block-' . $vars['zebra'];
	}

	/**
	 * Return a themed breadcrumb trail.
	 *
	 * @param $variables
	 * @return string A string containing the breadcrumb output.
	 * A string containing the breadcrumb output.
	 * @internal param $breadcrumb An array containing the breadcrumb links.*   An array containing the breadcrumb links.
	 */
	function dujardin_breadcrumb($variables) {
		$breadcrumb = $variables['breadcrumb'];  // Determine if we are to display the breadcrumb.
		$show_breadcrumb = theme_get_setting('dujardin_breadcrumb');
		if ($show_breadcrumb == 'yes' || ($show_breadcrumb == 'admin' && arg(0) == 'admin')) {


			// Optionally get rid of the homepage link.
			$show_breadcrumb_home = theme_get_setting('dujardin_breadcrumb_home');
			if (!$show_breadcrumb_home) {
				array_shift($breadcrumb);
			}
			// Return the breadcrumb with separators.
			if (!empty($breadcrumb)) {
				$breadcrumb_separator = theme_get_setting('dujardin_breadcrumb_separator');
				$trailing_separator = $title = '';
				if (theme_get_setting('dujardin_breadcrumb_title')) {
					$item = menu_get_item();
					if (!empty($item['tab_parent'])) {
						// If we are on a non-default tab, use the tab's title.
						$title = check_plain($item['title']);
					}
					else {
						$title = drupal_get_title();
					}
					if ($title) {
						$trailing_separator = $breadcrumb_separator;
					}
				}
				elseif (theme_get_setting('dujardin_breadcrumb_trailing')) {
					$trailing_separator = $breadcrumb_separator;
				}
				// Provide a navigational heading to give context for breadcrumb links to
				// screen-reader users. Make the heading invisible with .element-invisible.
				$heading = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

				return $heading . '<div class="breadcrumb">' . implode($breadcrumb_separator, $breadcrumb) . $trailing_separator . $title . '</div>';
			}
		}
		// Otherwise, return an empty string.
		return '';
	}

	/**
	 * Generate the HTML output for a menu link and submenu.
	 *
	 * @param $variables
	 *   An associative array containing:
	 *   - element: Structured array data for a menu link.
	 *
	 * @return
	 *   A themed HTML string.
	 *
	 * @ingroup themeable
	 */

	function dujardin_menu_link(array $variables) {
		$element = $variables['element'];
		$sub_menu = '';

		if ($element['#below']) {
			$sub_menu = drupal_render($element['#below']);
		}
		$output = l($element['#title'], $element['#href'], $element['#localized_options']);
		// Adding a class depending on the TITLE of the link (not constant)
		$element['#attributes']['class'][] = drupal_html_class($element['#title']);
		// Adding a class depending on the ID of the link (constant)
		if (isset($element['#original_link']['mlid']) && !empty($element['#original_link']['mlid'])) {
			$element['#attributes']['class'][] = 'mid-' . $element['#original_link']['mlid'];
		}
		return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
	}

	/**
	 * Override or insert variables into theme_menu_local_task().
	 */
	function dujardin_preprocess_menu_local_task(&$variables) {
		$link =& $variables['element']['#link'];

		// If the link does not contain HTML already, check_plain() it now.
		// After we set 'html'=TRUE the link will not be sanitized by l().
		if (empty($link['localized_options']['html'])) {
			$link['title'] = check_plain($link['title']);
		}
		$link['localized_options']['html'] = TRUE;
		$link['title'] = '<span class="tab">' . $link['title'] . '</span>';
	}

	/*
	 *  Duplicate of theme_menu_local_tasks() but adds clearfix to tabs.
	 */

	function dujardin_menu_local_tasks(&$variables) {
		$output = '';

		if (!empty($variables['primary'])) {
			$variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
			$variables['primary']['#prefix'] .= '<ul class="tabs primary clearfix">';
			$variables['primary']['#suffix'] = '</ul>';
			$output .= drupal_render($variables['primary']);
		}
		if (!empty($variables['secondary'])) {
			$variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
			$variables['secondary']['#prefix'] .= '<ul class="tabs secondary clearfix">';
			$variables['secondary']['#suffix'] = '</ul>';
			$output .= drupal_render($variables['secondary']);
		}

		return $output;

	}
