var dujardinApp = dujardinApp || {};

(function ($) {


    dujardinApp.contact = {

        init: function () {
            this.loadMap();
        },

        loadMap: function () {

            var center = new google.maps.LatLng(50.8455111, 4.34594);
            var mapOptions = {
                center: center,
                zoom: 17,
                zoomControl: true,
                navigationControl: false,
                scaleControl: true,
                scrollwheel: false,
                mapTypeControl: true
            };
           // var mapMarker = "/pressing/wp-content/themes/pressing/assets/images/mapMarker.png";
            var infowindow = new google.maps.InfoWindow({
                content: '<div class="results-info-window"><a href=""><h4>  DUJARDIN </h4><p>Avenue leMaître 34, 1060 Etterbeek</p></a></div>'
            });

            if ($("#map-canvas").length > 0) {
                var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
                var marker = new google.maps.Marker({
                    position: center,
                    map: map,
                   // icon: mapMarker,
                    animation: google.maps.Animation.DROP,
                    clickable: true
                });
                google.maps.event.addListener(marker, 'click', function () {

                    infowindow.open(map, marker);
                });

                google.maps.event.addDomListenerOnce(map, 'idle', function () {
                    google.maps.event.addDomListener(window, 'resize', function () {
                        map.setCenter(center);
                    });
                });
                // infowindow.open(map, marker);
            }
        }
    };

    dujardinApp.init = function () {
        this.contact.init();
    };

    dujardinApp.init();

})(jQuery);