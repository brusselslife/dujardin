'use strict';

var dujardinApp = dujardinApp || {};

(function ($) {
    dujardinApp.header = {
        init: function () {
            this.onClickScroll();
            this.mobileMenu();
        },
        onClickScroll: function () {
            $("header li:eq(4) a").click(function () {
                $.scrollTo("#map-canvas", {
                    duration: 900,
                    easing: "easeOutExpo",
                    offset: {
                        top: -100
                    }
                });

            });


            if (window.location.hash == "#contact") {
                $.scrollTo("#map-canvas", {
                    duration: 900,
                    easing: "easeOutExpo",
                    offset: {
                        top: -100
                    }
                });
            }


        },
        mobileMenu: function () {
            $(".menu-icon").click(function (e) {
                e.preventDefault();
                $(this).toggleClass("menu-icon-close");
                $("header nav").slideToggle(300, "easeOutExpo");
            });
        }
    };
    dujardinApp.dujardin = {
        init: function () {
            this.initCarousel();
        },
        initCarousel: function () {
            $(".home-slides").owlCarousel({
                singleItem: true,
                addClassActive: true,
                pagination: false
            });
        }
    };
    dujardinApp.contact = {
        init: function () {
            this.loadMap();
        },
        loadMap: function () {
            var center = new google.maps.LatLng(50.8181019,4.3798149);
            var mapOptions = {
                center: center,
                zoom: 15,
                zoomControl: true,
                navigationControl: false,
                scaleControl: true,
                scrollwheel: false,
                mapTypeControl: true
            };






            // var mapMarker = "/pressing/wp-content/themes/pressing/assets/images/mapMarker.png";
            var infowindow = new google.maps.InfoWindow({
                content: '<div class="results-info-window"><a href=""><h4>  Royale Dujardin SA </h4><p>Av. Adolphe Buyl 6<br>1050 Bruxelles<br> Tel : +32 2 554 18 60 <br> Fax : 32 2 648 47 57 <br> E-mail : info@royale-dujardin.be</p></a></div>'
            });
            if ($("#map-canvas").length > 0) {
                var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
                var marker = new google.maps.Marker({
                    position: center,
                    map: map,
                    // icon: mapMarker,
                    animation: google.maps.Animation.DROP,
                    clickable: true
                });
                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.open(map, marker);
                });
                google.maps.event.addDomListenerOnce(map, 'idle', function () {
                    google.maps.event.addDomListener(window, 'resize', function () {
                        map.setCenter(center);
                    });
                });
                // infowindow.open(map, marker);
            }
            if ($("#map-canvas-inside").length > 0) {
                var map = new google.maps.Map(document.getElementById("map-canvas-inside"), mapOptions);
                var marker = new google.maps.Marker({
                    position: center,
                    map: map,
                    // icon: mapMarker,
                    animation: google.maps.Animation.DROP,
                    clickable: true
                });
                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.open(map, marker);
                });
                google.maps.event.addDomListenerOnce(map, 'idle', function () {
                    google.maps.event.addDomListener(window, 'resize', function () {
                        map.setCenter(center);
                    });
                });
                // infowindow.open(map, marker);
            }
        }
    };
    dujardinApp.services = {
        init: function () {
            this.servicesAccordian();
            this.servicesCarousel();
        },
        servicesAccordian: function () {
            $("#service-list").find("a").click(function (e) {
                $("#service-list .item").removeClass("opened");
                e.preventDefault();
                $(this).parent().toggleClass("opened");
            });


        },
        servicesCarousel: function () {

//swiper-slide-active
            var swiper = new Swiper('.swiper-container', {
                pagination: '.swiper-pagination',
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev',
                slidesPerView: 5,
                paginationClickable: true,
                loop: true,
                speed:600,
                centeredSlides: true,
                breakpoints: {

                    320: {
                        slidesPerView: 1
                    },
                    480: {
                        slidesPerView: 1
                    },
                    640: {
                        slidesPerView: 1
                    },
                    768: {
                        slidesPerView: 2
                    },
                    1024: {
                        slidesPerView: 3
                    },
                    1199: {
                        slidesPerView: 4
                    }
                },

                onSlideChangeEnd: showActiveSlide
            });


            function showActiveSlide() {
                $(".services-carousel div.swiper-slide").removeClass("big");
                $(".services-carousel div.swiper-slide-active").addClass("big");
                console.log(0);
            }
        }
    };
    dujardinApp.init = function () {
        this.header.init();
        this.contact.init();
        this.dujardin.init();
        this.services.init();
    };
    $(document).ready(function () {
        dujardinApp.init();
    });
})(jQuery);