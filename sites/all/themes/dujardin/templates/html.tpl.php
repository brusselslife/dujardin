<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="<?php print $language->language; ?>"
      xml:lang="<?php print $language->language; ?>"
      dir="<?php print $language->dir; ?>" <?php print $rdf_namespaces; ?>> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang="<?php print $language->language; ?>"
      xml:lang="<?php print $language->language; ?>"
      dir="<?php print $language->dir; ?>" <?php print $rdf_namespaces; ?>> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>"
      dir="<?php print $language->dir; ?>" <?php print $rdf_namespaces; ?>> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>"
      dir="<?php print $language->dir; ?>" <?php print $rdf_namespaces; ?>> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
    <?php print $head; ?>
    <title><?php print $head_title; ?></title>
    <meta name="viewport"
          content="width=device-width initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no"/>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <link rel="apple-touch-icon" sizes="57x57"
          href="/<?php print drupal_get_path('theme', 'dujardin'); ?>/assets/images/favicons/apple-icon-57x57.png"/>
    <link rel="apple-touch-icon" sizes="60x60"
          href="/<?php print drupal_get_path('theme', 'dujardin'); ?>/assets/images/favicons/apple-icon-60x60.png"/>
    <link rel="apple-touch-icon" sizes="72x72"
          href="/<?php print drupal_get_path('theme', 'dujardin'); ?>/assets/images/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76"
          href="/<?php print drupal_get_path('theme', 'dujardin'); ?>/assets/images/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114"
          href="/<?php print drupal_get_path('theme', 'dujardin'); ?>/assets/images/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120"
          href="/<?php print drupal_get_path('theme', 'dujardin'); ?>/assets/images/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144"
          href="/<?php print drupal_get_path('theme', 'dujardin'); ?>/assets/images/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152"
          href="/<?php print drupal_get_path('theme', 'dujardin'); ?>/assets/images/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180"
          href="/<?php print drupal_get_path('theme', 'dujardin'); ?>/assets/images/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"
          href="/<?php print drupal_get_path('theme', 'dujardin'); ?>/assets/images/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32"
          href="/<?php print drupal_get_path('theme', 'dujardin'); ?>/assets/images/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96"
          href="/<?php print drupal_get_path('theme', 'dujardin'); ?>/assets/images/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16"
          href="/<?php print drupal_get_path('theme', 'dujardin'); ?>/assets/images/favicons/favicon-16x16.png">
    <!-- <link rel="manifest" href="/manifest.json"> -->
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <?php print $styles; ?>
    <?php print $scripts; ?>
</head>
<body class="<?php print $classes; ?>" <?php print $attributes; ?>>
<!--[if lt IE 7]>
<p class="chromeframe">You are using an<strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to
    improve your experience.</p>
<![endif]-->
<div id="skip" class="hidden">
    <a href="#main-menu"><?php print t('Jump to Navigation'); ?></a>
</div>

<main>
    <?php print $page_top; ?>
    <?php print $page; ?>
    <?php print $page_bottom; ?>
</main>

</body>
</html>
