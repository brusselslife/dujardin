<section>
	<div class="row">
		<img src="/<?php print drupal_get_path('theme', 'dujardin'); ?>/assets/images/food1.jpg" class="food1" alt="">
		<img src="/<?php print drupal_get_path('theme', 'dujardin'); ?>/assets/images/food2.jpg" class="food2" alt="">
		<div class="container">
			<div class="col-md-6 col-md-offset-3">
				<div class="welcome-text">
					<?php print $description['content']; ?>
				</div>
				<section id="service-list">
                    <?php if (isset($nodes) && !empty($nodes)): ?>
                        <?php foreach ($nodes as $key => $node): ?>
                            <div class="item <?php print !$key ? '' : ''; ?>">
                                <a href="#"><?php print $node['#node']->title; ?></a>
                                <p><?php print strip_tags(drupal_render($node['body'])); ?></p>
                            </div>
                        <?php endforeach; ?>
                    <?php endif ?>
				</section>
			</div>
		</div>
	</div>
</section>