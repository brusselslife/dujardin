<header class="scroll">
   <section class="container">
       <?php if ($logo): ?>
           <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"
              rel="home" id="logo">
               <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>"/>
           </a>
       <?php endif; ?>
       <div class="menu-icon hidden-lg hidden-md hidden-sm visible-xs">
           <span class="lines"></span>
       </div>
       <?php if ($main_menu): ?>

           <?php if ($page['language']): ?>
               <?php print render($page['language']); ?>
           <?php endif; ?>

           <nav id="navigation"
                class="menu <?php !empty($main_menu) ? print "with-primary" : '';
                !empty($secondary_menu) ? print " with-secondary" : ''; ?>">
               <?php print theme('links', array(
                   'links' => $main_menu,
                   'attributes' => array(
                       'id' => 'primary',
                       'class' => array('links', 'clearfix', 'main-menu')
                   )
               )); ?>


           </nav>
       <?php endif; ?>
   </section>
</header>
<article>
    <?php if ($page['header']): ?>
        <?php print render($page['header']); ?>
    <?php endif; ?>
    <section class="container">
        <div class="row">
            <div class="home-slides">
                <div class="slide">
                <?php if ($page['slide_1']): ?>
                    <?php print render($page['slide_1']) ?>
                <?php endif; ?>
                <?php if ($page['slide_2']): ?>
                    <?php print render($page['slide_2']) ?>
                <?php endif; ?>
                <?php if ($page['slide_3']): ?>
                    <?php print render($page['slide_3']) ?>
                <?php endif; ?>
                <?php if ($page['slide_4']): ?>
                    <?php print render($page['slide_4']) ?>
                <?php endif; ?>
                </div>
            </div>
        </div>
    </section>
    <?php if ($messages || $tabs || $action_links): ?>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php print $messages; ?>
                    <?php print render($page['help']); ?>
                    <?php if ($tabs): ?>
                        <div class="tabs"><?php print render($tabs); ?></div>
                    <?php endif; ?>
                    <?php if ($action_links): ?>
                        <ul class="action-links"><?php print render($action_links); ?></ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <?php if ($page['content_top']): ?>
        <?php print render($page['content_top']) ?>
    <?php endif; ?>
    <?php if ($page['lesprit_content_top']): ?>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <?php print render($page['lesprit_content_top']) ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <section>
        <?php print render($page['content']) ?>
    </section>
    <?php print $feed_icons; ?>
    <?php if ($page['content_bottom']): ?>
        <?php print render($page['content_bottom']) ?>
    <?php endif; ?>

    <?php if ($page['lesprit_content_bottom']): ?>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <?php print render($page['lesprit_content_bottom']) ?>
                </div>
            </div>
        </div>
    <?php endif; ?>






</article>

<?php if ($page['footer']): ?>
    <footer>
        <section class="container">
            <div class="row">
                <?php foreach ($page['footer'] as $block): ?>
                    <?php if (isset($block['#markup'])) {
                        print $block['#markup'];
                    } ?>
                <?php endforeach; ?>
            </div>
        </section>
    </footer>
<?php endif; ?>


