
    <div class="node-inner">

        <div class="content">

            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="welcome-text">
                            <?php
                           // print html_entity_decode($description['content']);
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <section id="block-carousel" style="padding:100px 0px;">
                <div class="services-carousel">

                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            <?php if (isset($nodes) && !empty($nodes)): ?>
                                <?php foreach ($nodes as $key => $node):?>
                                        <div class="swiper-slide">

                                            <?php if (isset($node['#node']->field_image['und'][0]['uri'])): ?>

                                            <img
                                                src="<?php print file_create_url($node['#node']->field_image['und'][0]['uri']); ?>"
                                                alt="">
                                            <?php endif; ?>
                                            <div class="info">
                                                <h3><?php print ($node['field_image']['#object']->title); ?></h3>
                                                <p>
                                                    <?php print ($node['field_image']['#object']->body['und'][0]['value']); ?>
                                                </p>
                                                <?php if (isset($node['#node']->field_employee_signature['und'][0]['uri'])): ?>
                                                <img
                                                    src="<?php print file_create_url($node['#node']->field_employee_signature['und'][0]['uri']); ?>"
                                                    alt=""/>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </div>
                        <!-- Add Pagination -->
<!--                        <div class="swiper-pagination"></div>-->
                        <!-- Add Arrows -->
                        <div class="next swiper-button-next"></div>
                        <div class="prev swiper-button-prev" style="left:0px;"></div>
                    </div>



                </div>
            </section>



                    <div class="row">
                        <img src="/<?php print drupal_get_path('theme', 'dujardin'); ?>/assets/images/knife.jpg"
                             class="fish" alt="">
                        <img src="/<?php print drupal_get_path('theme', 'dujardin'); ?>/assets/images/fish.jpg"
                             class="net" alt="">
                    </div>


            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="welcome-text">
                            <?php
                            // print $description['content'];
                            ?>
                        </div>
                    </div>
                </div>
            </div>



        </div>

    </div> <!-- /node-inner -->

